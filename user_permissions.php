<?php
error_reporting(0);
require_once('pdo_functions.php');
require_once('vendor/autoload.php');
session_start();
$_SESSION["state"] = "not_loggedIn";
$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

// funtion - create MD5 hashcode
$md5Filter = new Twig_SimpleFilter('md5', function($string) {
    return md5($string);
});
$twig->addFilter($md5Filter);

// function - render permissions (snippet)
$id_perm_function = new Twig_SimpleFilter('id_perm_function', function($array) {
    foreach ($array as $permission)
    {
      if ($permission == 1)
          echo "<div class=permission>lesen</div>";
      if ($permission == 2)
          echo "<div class=permission>schreiben</div>";
      if ($permission == 3)
          echo "<div class=permission>drucken</div>";
      if ($permission == 4)
          echo "<div class=permission>Benutzer verwalten</div>";    
    }
});
$twig->addFilter($id_perm_function);

// set the different states = ... ->
// "not_loggegIn", "logged_in", "manage", "createUser", "editUser", "editUserWrong", "dataSaved", "userDeleted"
////////////////////////////////////////////////////////////////////////////////

if (empty($_POST)){
    echo $twig->render('index.html', array(
        'state' => 'not_loggedIn',
    ));
}

if (isset($_POST["login"])){
    get_data_pdo();
    if ($_SESSION["state"] == "loggedIn"){
        echo $twig->render('index.html', array(
            'state' => 'logged_in',
            'SESSION_name' => $_SESSION["name"],
            'SESSION_surname' => $_SESSION["surname"],
            'permission_data' => $id_permission,
            'superUser' => $superUser,
        ));
    }
    else{
        echo $twig->render('index.html', array(
            'state' => 'wrongUser',
        ));
    }                 
}
        
if (isset($_POST["back"])){
    unset($_POST);   
    get_data_pdo();
    if ($_SESSION["state"] == "loggedIn"){
        echo $twig->render('index.html', array(
            'state' => 'logged_in',
            'SESSION_name' => $_SESSION["name"],
            'SESSION_surname' => $_SESSION["surname"],
            'permission_data' => $id_permission,
            'superUser' => $superUser,
        ));
    }
}

if (isset($_POST["back2"])){
    echo "<div class='frame'>";  
    echo "<h1>Alle Benutzer und deren Gruppen</h1>";  
    echo "<form action=".$_SERVER["PHP_SELF"]." method='post'>";
?>
    <table id="table_id" class="display">
    <thead>
        <tr>
            <th>Vorname</th>
            <th>Nachname</th>
            <th>Gruppe</th>
            <th>Benutzer bearbeiten</th>
        </tr>
    </thead>
    <tbody>
<?php
    show_users_pdo();
    ?>
    </tbody>
</table>
<?php
    echo $twig->render('index.html', array(
        'state' => 'manage',
    ));
}
                
if (isset($_POST["manageUser"])){
    echo "<div class='frame'>";  
    echo "<h1>Alle Benutzer und deren Gruppen</h1>";  
    echo "<form action=".$_SERVER["PHP_SELF"]." method='post'>";
?>
    <table id="table_id" class="display">
    <thead>
        <tr>
            <th>Vorname</th>
            <th>Nachname</th>
            <th>Gruppe</th>
            <th>Benutzer bearbeiten</th>
        </tr>
    </thead>
    <tbody>
<?php
    show_users_pdo();
    ?>
    </tbody>
</table>
<?php
    echo $twig->render('index.html', array(
        'state' => 'manage',
    ));
}
   
if (isset($_POST["logout"])){
    echo $twig->render('index.html', array(
        'state' => 'not_loggedIn',
    ));
    session_destroy();  
    $_SESSION = array();
    unset($_POST);
}

if (isset($_POST["createUser"])){
    echo $twig->render('index.html', array(
        'state' => 'createUser',
    ));
}
    
if (isset($_REQUEST["edit"])){
    $submitNumber = array_pop(array_keys($_REQUEST['edit']));
    $_SESSION["submitNumber"] = $submitNumber;
    edit_user();
    $sel = $_SESSION["submitNumber"];
    echo $twig->render('index.html', array(
        'state' => 'editUser',
        'surname' => $name_array[$sel],
        'firstname' => $firstname_array[$sel],
        'username' => $username_array[$sel],
        'passwd' => $passwd_array [$sel],
        'id_group' => $id_group,
    ));
}
    
if (isset($_POST["saveNewUser"])){
    global $username, $passwd, $surname, $firstname, $id_group, $username_invalid;

    $username = $_POST["user"];
    $passwd = $_POST["passwd"];
    $surname = $_POST["surname"];
    $firstname = $_POST["firstname"];
    $id_group = $_POST["group"];

    // translate password into hashcode 
    $passwdArray = ['userPasswd'];
    $passwd_hash = password_hash($passwd, PASSWORD_BCRYPT, $passwdArray);
    
    $pdo = connect_DB_pdo();

    // check if username already exists
    $sql = 'SELECT * from benutzer';
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $row_count = $stmt->rowCount();

    if ($row_count > 0){
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($results as $res)
        {
            $username_array[] = $res['benutzername'];
        }
    }
    else{
        echo "no data found in benutzer";
    }

    for ($i = 0; $i< count($username_array); $i++)
    {
        if ($username_array[$i] == $username){
            $username_invalid = true;
        }
    }

    if ($_POST["passwdRepeat"] != $_POST["passwd"]){
        echo $twig->render('index.html', array(
            'state' => 'editUserWrongPasswd',
            'surname' => $_POST["surname"],
            'firstname' => $_POST["firstname"],
            'username' => $_POST["user"],
            'passwd' => $_POST["passwd"],
            'id_group' => $_POST["group"],
            'button_check' => true,
        )); 
    }
    elseif ($username_invalid == true){
        echo $twig->render('index.html', array(
            'state' => 'editUserWrongInvalidUser',
            'surname' => $_POST["surname"],
            'firstname' => $_POST["firstname"],
            'username' => $_POST["user"],
            'passwd' => $_POST["passwd"],
            'id_group' => $_POST["group"],
            'button_check' => true,
        ));
    }       
    else{
             // insert new user into database
            $sql = 'INSERT INTO benutzer(benutzername, passwort, name, vorname) values(?, ?, ?, ?)';
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$username, $passwd_hash, $surname, $firstname]);
        
            $sql = 'SELECT id FROM benutzer WHERE name = ? && vorname = ?';
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$surname, $firstname]);
            $userID = $stmt->fetch(PDO::FETCH_OBJ);
          
            $sql = 'INSERT INTO rel_benutzer_gruppe(id_gruppen, id_benutzer) VALUES(?, ?)';
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$id_group, $userID->id]);
               
            echo $twig->render('index.html', array(
                'state' => 'dataSaved',
                'firstname' => $firstname,
                'surname' => $surname,         
            ));
        }
    $pdo = null;
}
    
if (isset($_POST["saveUser"])){
    global $id_array, $username, $passwd, $surname, $firstname, $id_group, $username_invalid;

    $username = $_POST["user"];
    $passwd = $_POST["passwd"];
    $surname = $_POST["surname"];
    $firstname = $_POST["firstname"];
    $id_group = $_POST["group"];

    // translate password into hashcode 
    $passwdArray = ['userPasswd'];
    $passwd_hash = password_hash($passwd, PASSWORD_BCRYPT, $passwdArray);
    
    $pdo = connect_DB_pdo();

    $sql = 'SELECT * from benutzer';
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $row_count = $stmt->rowCount();

    if ($row_count > 0){
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($results as $res)
        {
            $id_array[] = $res['id'];
            $username_array[] = $res['benutzername'];

        }  
    }
    else{
        echo "no data found in benutzer";
    }
    // check if username already exists
    for ($i = 0; $i < count($username_array); $i++)
    {
        if ($username_array[$i] == $username && $id_array[$i] != $id_array[$_SESSION["submitNumber"]]){
            $username_invalid = true;
        }
    }

    if ($username_invalid == true){
        echo $twig->render('index.html', array(
            'state' => 'editUserWrongInvalidUser',
            'surname' => $_POST["surname"],
            'firstname' => $_POST["firstname"],
            'username' => $_POST["user"],
            'passwd' => $_POST["passwd"],
            'id_group' => $_POST["group"],
            'button_check' => true,
        ));
        }  
    elseif ($_POST["passwdRepeat"] != $_POST["passwd"]){
            edit_user();
            $sel = $_SESSION["submitNumber"];
            echo $twig->render('index.html', array(
                'state' => 'editUserWrongPasswd',
                'surname' => $name_array[$sel],
                'firstname' => $firstname_array[$sel],
                'username' => $username_array[$sel],
                'passwd' => $passwd_array [$sel],
                'id_group' => $id_group,
            ));
    }         
    else{
            // update user un database
            $id = $id_array[$_SESSION["submitNumber"]];

            $sql = 'UPDATE benutzer SET benutzername = ?, passwort = ?, name = ?, vorname = ? WHERE id = ?';
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$username, $passwd_hash, $surname, $firstname, $id]);
            
            $sql = 'UPDATE rel_benutzer_gruppe SET id_gruppen = ? WHERE id_benutzer = ?';
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$id_group, $id]);

            echo $twig->render('index.html', array(
                'state' => 'dataSaved',
                'firstname' => $firstname,
                'surname' => $surname,              
            ));
    }
    $pdo = null;   
}

if (isset($_POST["deleteUser"])){
    delete_user_pdo();
    echo $twig->render('index.html', array(
        'state' => 'userDeleted',
        'firstname' => $firstname,
        'surname' => $surname,       
    ));
}

